package Animals;

import Sounds.Visitor;

public interface Animal {
    String getBreed();
    void accept(Visitor v);
}
