package Animals;

import Sounds.Visitor;

public class Cat implements Animal{
    public String getBreed() {
        return "Cat";
    }

    public void accept(Visitor v) {
        v.visitAnimal(this);
    }
}
