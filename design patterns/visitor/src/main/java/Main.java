import Animals.Animal;
import Animals.Cat;
import Animals.Dog;
import Sounds.MakeSoundVisitor;
import Sounds.SurpriseVisitor;
import Sounds.Visitor;

public class Main {
    public static void main(String[] args) {
        Visitor visitor1 = new MakeSoundVisitor();
        Visitor visitor2 = new SurpriseVisitor();

        Animal dog = new Dog();
        Animal cat = new Cat();

        dog.accept(visitor1);
        cat.accept(visitor1);
        dog.accept(visitor2);
        cat.accept(visitor2);
    }
}
