package Sounds;

import Animals.Cat;
import Animals.Dog;

public class MakeSoundVisitor implements Visitor {

    public void visitAnimal(Cat cat) {
        System.out.println("Cat goes Meow");
    }

    public void visitAnimal(Dog dog) {
        System.out.println("Dog goes Bark");
    }
}
