package Sounds;

import Animals.Cat;
import Animals.Dog;

public class SurpriseVisitor implements Visitor {
    public void visitAnimal(Cat cat) {
        System.out.println("Surprised cat does Reee");
    }

    public void visitAnimal(Dog dog) {
        System.out.println("Surprised dog does Wof Wof");
    }
}
