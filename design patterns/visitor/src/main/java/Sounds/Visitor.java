package Sounds;

import Animals.Cat;
import Animals.Dog;

public interface Visitor {
    void visitAnimal(Cat cat);
    void visitAnimal(Dog dog);
}
