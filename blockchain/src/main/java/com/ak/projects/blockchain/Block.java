package com.ak.projects.blockchain;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;
import java.util.logging.Level;

import static java.nio.charset.StandardCharsets.UTF_8;

public class Block {
    private String hash;
    private String previousHash;
    private Transaction transaction;
    private long timeStamp;
    private int nonce;

    public Block(String previousHash, Transaction transaction, long timeStamp) {
        this.previousHash = previousHash;
        this.transaction = transaction;
        this.timeStamp = timeStamp;
        this.hash = calculateBlockHash();
    }

    /**
     * Calculates a hash for the content of the block
     * @return String hash represented by 32-digit hex number
     */
    public String calculateBlockHash(){
        String dataToHash = previousHash + Long.toString(timeStamp) + Integer.toString(nonce) + transaction.toString();
        MessageDigest digest = null;
        byte[] bytes = null;

        try{
            digest = MessageDigest.getInstance("SHA-256");
            bytes = digest.digest(dataToHash.getBytes(UTF_8));
        } catch (NoSuchAlgorithmException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        StringBuilder buffer = new StringBuilder();
        for (byte b : Objects.requireNonNull(bytes)){
            buffer.append(String.format("%02x", b));
        }
        return buffer.toString();
    }


    public String mineBlock(int prefix){
        String prefixString = new String(new char[prefix]).replace('\0','0');
        while (!hash.substring(0,prefix).equals(prefixString)){
            nonce++;
            hash = calculateBlockHash();
        }
        return hash;
    }


    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getPreviousHash() {
        return previousHash;
    }

    public void setPreviousHash(String previousHash) {
        this.previousHash = previousHash;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getNonce() {
        return nonce;
    }

    public void setNonce(int nonce) {
        this.nonce = nonce;
    }
}
