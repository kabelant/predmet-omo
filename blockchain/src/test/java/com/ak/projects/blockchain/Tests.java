package com.ak.projects.blockchain;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Tests {
    Transaction transaction1 = new Transaction("Pepa", "Jarda",69);
    Transaction transaction2 = new Transaction("Vaclav", "Jenda",420);

    List<Block> blockchain = new ArrayList<>();
    int prefix = 4;
    String prefixString = new String(new char[prefix]).replace('\0','0');

    @Before
    public void initBlockChain(){
        Block firstBlock = new Block(
                "0",
                transaction1,
                new Date().getTime()
        );
        firstBlock.mineBlock(prefix);

        blockchain.add(firstBlock);
    }

    @Test
    public void givenBlockChain_whenNewBlockAdded_thenSuccess(){
        Block newBlock = new Block(
                blockchain.get(blockchain.size() - 1).getHash(),
                transaction2,
                new Date().getTime()
        );

        newBlock.mineBlock(prefix);
        assertEquals(newBlock.getHash().substring(0, prefix), prefixString);
        blockchain.add(newBlock);
    }

    @Test
    public void givenBlockChain_whenValidated_thenSuccess(){
        boolean flag = true;
        for (int i = 0; i < blockchain.size(); i++){
            String previousHash = i == 0 ? "0" : blockchain.get(i - 1).getHash();
            flag = blockchain.get(i).getHash().equals(blockchain.get(i).calculateBlockHash())
                    && previousHash.equals(blockchain.get(i).getPreviousHash())
                    && blockchain.get(i).getHash().substring(0, prefix).equals(prefixString);
            if(! flag) break;
        }
        assertTrue(flag);
    }
}
